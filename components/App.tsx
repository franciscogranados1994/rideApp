import React from 'react';

const App = () => {
  function setData(e) {
    let lengthRequired;
    const { value } = e.target;

    if (value.length === 0) {
      lengthRequired = false;
    } else {
      lengthRequired = true;
    }

    return lengthRequired;
  }

  return (
    <label>
      <input
        type="checkbox"
        onChange={(e) => {
          setData(e);
        }}
      />
      Hello
    </label>
  );
};

export default App;
