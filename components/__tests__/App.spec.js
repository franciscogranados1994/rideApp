import { shallow } from "enzyme";
import React from "react";
import renderer from "react-test-renderer";
import { mount } from 'enzyme';

import App from '../App'
/* 
describe("With Enzyme", () => {
  it('App shows "App configuration"', () => {
    const app = mount(<App />);

    expect(app.find("div").text()).toEqual("App configuration");
  });
}); */

/* 
export default class CheckboxWithLabel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isChecked: false};

    // bind manually because React class components don't auto-bind
    // https://reactjs.org/blog/2015/01/27/react-v0.13.0-beta-1.html#autobinding
    this.onChange = this.onChange.bind(this);
  }

  onChange() {
    this.setState({isChecked: !this.state.isChecked});
  }

  render() {
    return (
      <label>
        <input
          type="checkbox"
          checked={this.state.isChecked}
          onChange={this.onChange}
        />
        {this.state.isChecked ? this.props.labelOn : this.props.labelOff}
      </label>
    );
  }
}
 */

 test('CheckboxWithLabel changes the text after click', () => {
  // Render a checkbox with label in the document
  const checkbox = shallow(<App/>);

  checkbox.find('input').simulate('change',{ target: { value: '23' } } );
  
  expect(checkbox).toBe(true)
});
 